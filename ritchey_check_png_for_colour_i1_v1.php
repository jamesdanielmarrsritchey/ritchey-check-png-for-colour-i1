<?php
#Name:Ritchey Check PNG For Colour i1 v1
#Description:Check a PNG for instances of a particular colour. Returns array of locations if colour is present. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. 
#Arguments:'source' (required) is a path to a PNG. 'colour' (required) is a string of the colour to search for in RGB format (e.g., 'rgb(255,255,255)'). 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,colour:string:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_check_png_for_colour_i1_v1') === FALSE){
function ritchey_check_png_for_colour_i1_v1($source, $colour, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@is_file($source) === FALSE){
		$errors[] = "source";
	}
	if (@isset($colour) === FALSE){
		$errors[] = "colour";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Extract the colour of each pixel, and check if it matches the colour being checked for.]
	if (@empty($errors) === TRUE){
		###Import PNG
		$png = @file_get_contents($source);
		###Convert PNG data to image
		$image = @imagecreatefromstring($png);
		###Determine image width and height
		$width = @imagesx($image);
		$height = @imagesy($image);
		###Get each pixel color as RGB, and save to an array if matches colour.
		$result = array();
		$total_pixels = $width * $height;
		$pixel_x_coordinate = 0;
		$pixel_y_coordinate = 0;
		$pixel_number = 0;
		for ($i = 0; $i < $total_pixels; $i++) {
			$pixel_number = $i + 1;
			$pixel_colour_index = @imagecolorat($image, $pixel_x_coordinate, $pixel_y_coordinate);
			$pixel_colour_rgb = @imagecolorsforindex($image, $pixel_colour_index);
			$pixel_x_coordinate_human_readable = $pixel_x_coordinate + 1;
			$pixel_y_coordinate_human_readable = $pixel_y_coordinate + 1;
			if ("rgb({$pixel_colour_rgb['red']},{$pixel_colour_rgb['green']},{$pixel_colour_rgb['blue']})" === $colour) {
				$result[] = "Pixel Number:{$pixel_number},XY Coordinates:{$pixel_x_coordinate_human_readable},{$pixel_y_coordinate_human_readable},Colour:rgb({$pixel_colour_rgb['red']},{$pixel_colour_rgb['green']},{$pixel_colour_rgb['blue']})";
			}
			$pixel_x_coordinate++;
			if ($pixel_x_coordinate >= $width){
				$pixel_x_coordinate = 0;
				$pixel_y_coordinate++;
			}
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_check_png_for_colour_i1_v1_format_error') === FALSE){
				function ritchey_check_png_for_colour_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_check_png_for_colour_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $result;
	} else {
		return FALSE;
	}
}
}
?>